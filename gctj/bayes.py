import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

data = pd.read_csv("data.csv", encoding='GBK') ## 导入数据集
# data.drop(index=[-1])
print(data)

result = np.array(data) ## 转换为dataframe格式
print(result)

# 分类目标和特征值
target = result[:, -1]
print('target', target)
feature = np.delete(result, -1, axis=1)
print('feature', feature)

# 切分数据集
X_train, X_test, y_train, y_test = train_test_split(feature, target, random_state=12)
print(len(X_train), len(feature))

# 建模
clf = GaussianNB()
clf.fit(X_train, y_train)
# 在测试集上执行预测，proba导出的是每个样本属于某类的概率
predict_result = clf.predict(X_test)
predict_rate = clf.predict_proba(X_test)
# 使用测试集测试准确率
accuracy = accuracy_score(y_test, predict_result)
print('准确率', accuracy)
