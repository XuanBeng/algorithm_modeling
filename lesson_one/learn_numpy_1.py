# 引入numpy全部 并且命名为 np
import numpy as np


# 读取数据文件：使用load方法
# load 读取 .npz 文件 loadtxt 读取 .txt 文件
world_alcohol = np.loadtxt('../static/world_alcohol.txt', delimiter=',' ,dtype = str)

# ndarray n维数组对象
# 创建一维数组
nd_array1 = np.array([1, 2, 3])

# 使用ndmin 控制最小维度
nd_array2 = np.array( [[1, 2, 3],[4, 5, 6]], ndmin=3)

# 创建随机矩阵 empty(shape, dtype=None, order='C') dtype默认为float
empty_array =  np.empty((3,3), dtype = int)

# 创建0矩阵, zeros(shape, dtype = None, order='C') dtype默认为float
zeros_array = np.zeros((3,3), dtype = int)

# 创建全为1矩阵， ones(shape, dtype=None, order='C') dtype默认为float
ones_array = np.ones((3,3), dtype = int)

# 创建arange数组
random_array = np.arange(1,5).reshape((2,2))

# 查看矩阵结构
shape = random_array.shape

a = np.arange(9, dtype = np.int).reshape(3,3)
b = np.array([10,10,10])
# 相加
add = np.add(a,b)
# 相减
subtract = np.subtract(a,b)
# 相乘
multiply = np.multiply(a,b)
# 相除
divide = np.divide(a,b)

# 拼接 concatenate(): 连接沿现有轴的数组序列
#      stack(): 沿着新的轴加入一系列数组。
#      hstack(): 水平堆叠序列中的数组（列方向）
#      vstack(): 竖直堆叠序列中的数组（行方向）
concat = np.concatenate((zeros_array, ones_array))

# 拆分 split(): 将一个数组分割为多个子数组
#      hsplit(): 将一个数组水平分割为多个子数组（按列）
#      vsplit(): 将一个数组垂直分割为多个子数组（按行）
split = np.split(ones_array, 3)
print(split)