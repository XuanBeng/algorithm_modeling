import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
from sklearn import preprocessing as pp


# Q1.逻辑回归的目标函数是求最大值，如何进行梯度下降？
# 在目标函数前乘一个-1/m，使得原本求极大值，转换为求极小值，从而变成梯度下降。

# Q2.逻辑回归中的参数更新，方向和步长各为什么？
# 方向 （h_0(x_i) - y_i）* x_i ^ j ; 步长：a （_代表下表,^代表上标）

# Q3.梯度下降的下山过程中如何满足走一小步的要求？
# 第一步首先要找到行走的方向，一般都是该点的切线方向（迭代速度最快方向最优）；第二步，按照
# 方向走一小步（称为学习率）；第三步，迭代，每走一步更新一下方向

# Q4.读取数据 LogiReg_data.txt 时，你所用的 path 是什么？
path ='../static/LogiReg_data.txt'
data = pd.read_csv(path, header=None, names=['Exam 1', 'Exam 2', 'Admitted'])
# print(data.head())

positive = data[data['Admitted'] == 1]
negative = data[data['Admitted'] == 0]
# Q5.请把 print (positive) 和 print (negative)的结果截图。
# print(positive)
# print(negative)

# Q6. figsize=(10,5)中长和宽分别等于多少 ，单位是什么？
# 长10 宽5; 单位：英寸
# fig,ax = plt.subplots(figsize = (10, 5))
# ax.scatter(positive['Exam 1'], positive['Exam 2'], s=30, c='b', marker='o', label= 'Admitted')
# ax.scatter(negative['Exam 1'], negative['Exam 2'], s=30, c='r', marker='x', label= 'Not Admitted')
# ax.legend()
# ax.set_xlabel('Exam 1 Score')
# ax.set_ylabel('Exam 2 Score')


def sigmoid(z):
    return 1/(1+ np.exp(-z))

# Q7. numpy中的矩阵相乘函数是什么？ np.dot()函数
def model(X, theta):
    return sigmoid(np.dot(X, theta.T))

# Q9. 请解释 cost（X，y， theta）函数每一行实现了哪部分数学模型计算。
def cost(X, y, theta):
    left = np.multiply(-y,np.log(model(X,theta))) #左半边公式
    right = np.multiply(1-y, np.log(1- model(X, theta))) #右半边公式
    result = np.sum(left-right) / len(X) #累加
    return result

# Q10. 请解释 gradient（X，y， theta）函数每一行实现了内容。
def gradient(X, y, theta):
    grad = np.zeros(theta.shape) #初始化梯度
    error = (model(X, theta)- y).flatten() # 求导项；这里应该使用flatten()函数
    for j in range(len(theta.ravel())): #使用for循环对每个theta求导
        term = np.multiply(error, X[:, j])
        grad[0, j] = np.sum(term)/ len(X)

    return grad

# 三种停止策略
STOP_ITER = 0
STOP_COST = 1
STOP_GRAD = 2

# 选择某种策略
def stopCriterion(type, value, threshold):
    # 设定三种不同的策略
    if type == STOP_ITER:
        return value > threshold
    elif type == STOP_COST:
        return abs(value[-1] - value[-2]) < threshold
    elif type == STOP_GRAD:
        return np.linalg.norm(value) < threshold

# 洗牌函数（打乱数据的顺序）
def shuffleData(data):
    np.random.shuffle(data)
    cols = data.shape[1]
    X = data[:, 0:cols - 1]
    y = data[:, cols - 1:]
    return X, y

# 梯度下降函数
def descent(data, theta, batchSize, stopType, thresh, alpha):
    # 初始化
    init_time = time.time()
    i = 0 # 迭代次数
    k = 0 # batch
    X, y = shuffleData(data)
    grad = np.zeros(theta.shape) # 计算损失的梯度
    costs = [cost(X, y, theta)] #损失值

    while True:
        grad = gradient(X[k: k+batchSize], y[k: k+batchSize], theta)
        k += batchSize # 取batch个数据
        if k >= N:  # 原来是n
            k = 0
            X, y = shuffleData(data)
        # 参数更新
        theta = theta - alpha*grad
        costs.append(cost(X, y, theta)) # 计算损失值并append
        i += 1
        print("更新",i)


        value = 0
        if stopType == STOP_ITER:
            value = i
        elif stopType == STOP_COST:
            value = costs
        elif stopType == STOP_GRAD:
            value = grad

        if stopCriterion(stopType, value, thresh):
            break

    return theta, i-1, costs, grad, time.time()-init_time

def runExpe(data, theta, batchSize, stopType, thresh, alpha):
    theta, iter, costs, grad, dur = descent(data, theta, batchSize, stopType, thresh, alpha)

    name = "Original" if (data[:, 1] > 2).sum() > 1 else "Scaled"
    name += "data - learning rate : {} - ".format(alpha)
    print("时间", dur)
    strDescType = ''
    if batchSize == N:
        strDescType = "Gradient"
    elif batchSize == N:
        strDescType = "Stochastic"
    else:
        strDescType = "Mini-batch({})".format(batchSize)

    fig, ax = plt.subplots(figsize = (12, 4))
    ax.plot(np.arange(len(costs)), costs, 'r')
    ax.set_xlabel("Iterations")
    ax.set_ylabel("Cost")
    plt.show()

    return theta

# nums = np.arange(-10,10,step = 1)
# fig,ax = plt.subplots(figsize = (12, 4))
# ax.plot(nums, sigmoid(nums), 'r')
# plt.show()



data.insert(0, 'Ones', 1)
# as_matrix()函数变为values
orig_data =data.values
cols = orig_data.shape[1]
X = orig_data[: ,0:cols-1]
y = orig_data[: ,cols-1 : cols]
theta = np.zeros([1,3])
N = len(X)
n = 100

# 全样本
# runExpe(orig_data, theta, n, STOP_ITER, thresh= 5000, alpha= 0.000001)
# runExpe(orig_data, theta, n, STOP_COST, thresh= 0.000001, alpha= 0.001)
# runExpe(orig_data, theta, n, STOP_GRAD, thresh= 0.05, alpha= 0.001)

# 随机
# runExpe(orig_data, theta, 1, STOP_ITER, thresh= 5000, alpha= 0.001)
# runExpe(orig_data, theta, 1, STOP_ITER, thresh= 15000, alpha= 0.000002)

# mini-batch
# runExpe(orig_data, theta, 16, STOP_ITER, thresh= 15000, alpha= 0.001)

scaled_data = orig_data.copy()
scaled_data[: , 1: 3] = pp.scale(orig_data[:, 1: 3 ])
runExpe(scaled_data, theta, 16, STOP_ITER, thresh= 5000, alpha= 0.001)


# 1.STOP_COST和STOP_ITER相比较，优缺点是什么？
# 优：迭代结果更加准确；缺：速度相对较慢

# 2.当n=1，STOP_ITER，thresh=5000的运行结果说明了什么问题？
# 迭代结果不稳定，波动非常大

# 3.使用sklearn库的目的是什么？
# 对数据进行标准化处理

# 4.用梯度下降求解逻辑回归的时候，需注意哪些方面从而快速高效得得到收敛解？
# 1.对数据先进行预处理; 2.尝试不同的迭代方法进行迭代,对结果进行比较.

# 5.请概述逻辑回归程序的主要模块及其所实现的功能。
# 见上程序详解注释

# 6.请解释下采样(under-sampling)和过采样(oversampling)的原理。
# 过采样oversampling：重复正比例数据，实际上没有为模型引入更多数据，过分强调正比例数据，会放大正比例噪音对模型的影响。
# 欠采样under-sampling：丢弃大量数据，和过采样一样会存在过拟合的问题。

print()