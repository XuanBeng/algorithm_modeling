import pandas as pd

# 读取数据
data = pd.read_csv("../static/food_info.csv")

# Shrt_Desc为字符串，但在pandas中为object类型
dtypes = data.dtypes

# 显示所有列信息
columns = data.columns


# 查看某一行
col =  data['Shrt_Desc']
# 取多列
cols = data[['Shrt_Desc', 'NDB_No']]

# 找结尾为（g）列的列名
col_list = columns.tolist()
gram_columns = []
for c in col_list:
    if c.endswith("(g)"):
        gram_columns.append(c)
col_g = data[gram_columns]

print(data.shape)
iron_g = data['Iron_(mg)'] / 1000
data['iron_(g)'] = iron_g
print(data.shape)

# NAN 表示缺失值