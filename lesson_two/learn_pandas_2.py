import pandas as pd
import numpy as np

titanic_train = pd.read_csv('../static/titanic_train.csv')

# 显示所有列信息
columns = titanic_train.columns
print(columns)

ages = titanic_train['Age']
ages_is_null = pd.isnull(ages)
ages_true = ages[ages_is_null]

age_average_nan = sum(ages)/len(ages) # 存在NAN，输出也为NAN
ages_no_nan = ages[ages_is_null == False]
age_average =sum(ages_no_nan) / len(ages_no_nan)
# print(age_average)

ticket = titanic_train['Ticket']
person_ticket = titanic_train.pivot_table(index='Pclass', values='Fare' ) # aggfunc默认为取平均值
# print(person_ticket)

# 使用dropna函数去除NAN
drop_nan_age = titanic_train.dropna(axis= 0, subset=['Name', 'Age'])
# print(drop_nan_age)


############################################

def firstRow(col):
    first_row = col.loc[0]
    return first_row

############################################

first_row = titanic_train.apply(firstRow)
print(first_row)



