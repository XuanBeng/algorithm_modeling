import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing as pp

from sklearn.model_selection import train_test_split

path ='../static_ignore/creditcard.csv'
data = pd.read_csv(path)

count_classes = pd.value_counts(data['Class'], sort=True).sort_index()
# count_classes.plot(kind = 'bar')
# plt.xlabel('class')
# plt.ylabel('Frequency')
# plt.show()

# data['normAmount'] = pp.StandardScaler().fit_transform(data['Amount'].reshape(-1, 1))
# data['Amount']为series对象,没有reshape方法,使用series的values方法属性转成ndarray类型就可以使用reshape方法了
data['normAmount'] = pp.StandardScaler().fit_transform(data['Amount'].values.reshape(-1, 1))
data = data.drop(['Time', 'Amount'], axis=1)

# X = data.ix[:, data.columns != 'Class']  ix属性已被删除，用lod或者iloc代替
X = data.loc[:, data.columns != 'Class']
Y = data.loc[:, data.columns == 'Class']

number_records_fraud = len(data[data.Class == 1])
fraud_indices = np.array(data[data.Class == 1].index)

normal_indices = data[data.Class == 0].index

# choice(a, size=None, replace=True, p=None): a代表样本，size代表大小，replace是否有重复
# 从normal_indices中选number_records_fraud个不重复数据重新组成一个narray
random_normal_indices = np.random.choice(normal_indices, number_records_fraud, replace=False)
random_normal_indices = np.array(random_normal_indices)

under_sample_indices = np.concatenate([fraud_indices, random_normal_indices])
under_sample_data = data.iloc[under_sample_indices, :]

X_under_sample = under_sample_data.loc[:, under_sample_data.columns != 'Class']
Y_under_sample = under_sample_data.loc[:, under_sample_data.columns == 'Class']

# 3.请解释训练集和测试集。
# 训练集：用于训练模型的子集；验证集：用于测试训练后模型的子集。

# 4.请解释交叉验证及其作用
# 交叉验证：在训练集中再划分出几个子集，轮流分配训练集和验证集，其中每次训练集占大比例，验证集占小比例。
# 作用：如果只做一次训练集和验证集的分配，会导致较大误差，交叉验证后能取平均值，让模型的评估效果更可信。

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=0)
# print(len(X_train))
# print(len(X_test))
# print(len(X_train)+ len(X_test))
X_under_s_train, X_under_s_test, y_under_s_train, y_under_s_test = train_test_split(X_under_sample, Y_under_sample, test_size=0.3, random_state=0)
# print(len(X_under_s_train))
# print(len(X_under_s_test))
# print(len(X_under_s_train)+ len(X_under_s_test))
#
# # five =  data.head()
# print(len(under_sample_data))




# 1.recall值是什么值？作用是什么？和精度的区别是什么？
# 召回率，查全率，
# 作用是计算模型的效果 recall=TP/(TP+FN)
# 精度等于预测正确的正类值/样本总数，这个数据当样本数据不均衡往往会骗人

# 2.TP是什么含义？FP是什么含义？FN是什么含义？TN是什么含义？
# TP表示把正类判断成了正类, FP表示把负类判断成了正类，FN表示把正类判断为负类，TN表示把负类判断为负类

# 5. 请解释过拟合。如何避免过拟合？
# 如果模型在训练集上表达得很好，在测试集表达得很差，则称之为发生了过拟合。是由于权重参数浮动过大引起的
# 如何避免: 引入正则化,惩罚函数惩罚浮动过大的权重参数

# 6. 请解释惩罚函数及其作用。
# 惩罚的作用是为了正则化，通过使用不同的c参数来获得稳定的模型

from sklearn.linear_model import LogisticRegression
# 3. from sklearn.cross_validation import KFold, cross_val_score
from sklearn.model_selection import KFold, cross_val_score
from sklearn.metrics import confusion_matrix, recall_score, classification_report


def print_kfold_scores(x_train_data, y_train_data):
    # fold = KFold(len(y_train_data), 5, shuffle=False)
    fold = KFold(5, shuffle=False)  # 数据数目，交叉折叠次数5次，不进行洗牌

    c_param_range = [0.01, 0.1, 1, 10, 100]  # 待选的模型参数
    # 新建一个dataFrame类型（csv，就像一个表格），列名是参数取值、平均召回率
    result_table = pd.DataFrame(index=range(len(c_param_range), 2), columns=['C_parameter', 'Mean recall score'])
    result_table['C_parameter'] = c_param_range

    j = 0
    for c_param in c_param_range:  # 将待选参数一个一个进行模型训练，并分别计算对应的召回率
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print('C parameter:', c_param)
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        recall_accs = []
        for iteration, indices in enumerate(fold.split(x_train_data), start=1):  # 交叉验证

            lr = LogisticRegression(C=c_param, penalty='l1', solver='liblinear')  # 实例化逻辑回归模型

            lr.fit(x_train_data.iloc[indices[0], :], y_train_data.iloc[indices[0], :].values.ravel())  # 将数据带入训练

            y_pred_undersample = lr.predict(x_train_data.iloc[indices[1], :].values)  # 预测结果

            recall_acc = recall_score(y_train_data.iloc[indices[1], :].values, y_pred_undersample)  # 召回率

            recall_accs.append(recall_acc)
            print('Iteration', iteration, ': recall score =', recall_acc)  # 交叉验证的折叠次数为5，有5次小结果

        result_table.loc[j, 'Mean recall score'] = np.mean(recall_accs)  # 计算该参数对应的平均召回率
        j += 1
        print('')
        print('Mean recall score', np.mean(recall_accs))
        print('')

    best_c = result_table.iloc[result_table['Mean recall score'].astype('float64').idxmax()]['C_parameter']

    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    print(' best is  = ', best_c)

    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    return best_c


best_c = print_kfold_scores(X_under_s_train, y_under_s_train)