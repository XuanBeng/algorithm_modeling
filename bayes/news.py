import pandas as pd
import numpy as np
import jieba
import matplotlib.pyplot as plt

# 文件路径
file_path = '../data/val.txt'
stop_path = '../data/stopwords.txt'


# 加载文件，并分成四列'category','theme','URL','content'，并用utf-8编码方式解码
def load_data(filename):
    df_news = pd.read_table(filename,sep='\t',names=['category','theme','URL','content'], encoding='utf-8')
    df_news = df_news.dropna()
    return df_news
df_news = load_data(file_path)
print(df_news.head())
print(df_news.shape)

# 转换为list 实际上是二维list
content = df_news.content.values.tolist()
# print(content[1000])

content_S = []
for line in content:
    # jieba分词 精确模式。返回一个列表类型，建议使用
    current_segment = jieba.lcut(line)
    if len(current_segment) > 1 and current_segment != '\r\n':
        content_S.append(current_segment)

print(content_S[1000])


df_content = pd.DataFrame({'content_S':content_S})
# print(df_content.head())

stopwords = pd.read_csv(stop_path,index_col=False,sep='\t',quoting=3,names=['stopword'],encoding='utf-8')
# print(stopwords.head())
# 删除新闻中的停用词
def drop_stopwords(contents, stopwords):
    contents_clean = [] # 删除后的新闻
    all_words = []  # 构造词云所用的数据
    for line in contents:
        line_clean = []
        for word in line:
            if word in stopwords:
                continue
            line_clean.append(word)
            all_words.append(str(word))
        contents_clean.append(line_clean)
    return contents_clean, all_words

contents = df_content.content_S.values.tolist()
stopwords = stopwords.stopword.values.tolist()
# 得到删除停用词后的新闻以及词云数据
contents_clean, all_words = drop_stopwords(contents, stopwords)
df_content = pd.DataFrame({'contents_clean':contents_clean})
print(df_content.head())

df_all_words = pd.DataFrame({'all_words':all_words})
df_all_words.loc[:, 'count'] = 1
# print(df_all_words.head())

data_group3 = df_all_words.groupby('all_words').agg({'count':'count'})
# words_count = df_all_words.groupby(by=['all_words'])['all_words'].agg({'count':np.size})
# 根据count排序
words_count = data_group3.reset_index().sort_values(by=['count'],ascending=False)
print(words_count.head())

from wordcloud import WordCloud
import matplotlib.pyplot as plt
# %matplotlib inline
import matplotlib
matplotlib.rcParams['figure.figsize'] = (10.0, 5.0)

wordcloud=WordCloud(font_path=sim_path, background_color="white", max_font_size=80)
word_frequence = {x[0]:x[1] for x in words_count.head(100).values}
wordcloud=wordcloud.fit_words(word_frequence)
plt.imshow(wordcloud)
plt.show()

import jieba.analyse #工具包
index = 345 #随便找一篇文章就行
content_S_str = "".join(content_S[index]) #把分词的结果组合在一起，形成一个句子
# print (content_S_str) #打印这个句子
# print ("  ".join(jieba.analyse.extract_tags(content_S_str, topK=5, withWeight=False)))#选出来5个核心词

from gensim import corpora,models,similarities
import gensim
dictionary = corpora.Dictionary(contents_clean) # 字典
corpus = [dictionary.doc2bow(sentence) for sentence in contents_clean] # 语料
lda = gensim.models.ldamodel.LdaModel(corpus=corpus,id2word=dictionary,num_topics=20)

df_train=pd.DataFrame({'contents_clean':contents_clean,'label':df_news['category']})
# print(df_train.tail())
print(df_train.label.unique())

label_mapping = {"汽车": 1, "财经": 2, "科技": 3, "健康": 4, "体育":5, "教育": 6,"文化": 7,"军事": 8,"娱乐": 9,"时尚": 0}
df_train['label'] = df_train['label'].map(label_mapping)
# print(df_train.head())

from sklearn.model_selection import train_test_split

x_train, x_test, y_train, y_test = train_test_split(df_train['contents_clean'].values, df_train['label'].values,
                                                    random_state=1)
# print(x_train[0][1])
words = []
for line_index in range(len(x_train)):
    try:
        #x_train[line_index][word_index] = str(x_train[line_index][word_index])
        words.append(' '.join(x_train[line_index]))
    except:
        print(line_index)
print(words[0])
# print(len(words))
from sklearn.feature_extraction.text import CountVectorizer
vec = CountVectorizer(analyzer='word', max_features=4000, lowercase=False)
vec.fit(words)
from sklearn.naive_bayes import MultinomialNB
classifier = MultinomialNB()
# print('vec.transform(words)', vec.transform(words))
classifier.fit(vec.transform(words), y_train)

test_words = []
for line_index in range(len(x_test)):
    try:
        #x_train[line_index][word_index] = str(x_train[line_index][word_index])
        test_words.append(' '.join(x_test[line_index]))
    except:
         print (line_index)
# test_words[0]
classifier.score(vec.transform(test_words), y_test)

print('score', classifier.score(vec.transform(test_words), y_test))
