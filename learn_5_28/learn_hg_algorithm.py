import numpy as np
import pandas as pd

pd.set_option('display.max_columns',1000)
pd.set_option('display.width', 1000)
pd.set_option('display.max_colwidth',1000)
titanic = pd.read_csv("../static_ignore/titanic_train.csv")

titanic["Age"] = titanic["Age"].fillna(titanic["Age"].median())

titanic.loc[titanic["Sex"] == "male", "Sex"] = 0
titanic.loc[titanic["Sex"] == "female", "Sex"] = 1

titanic["Embarked"] = titanic["Embarked"].fillna('S')
titanic.loc[titanic["Embarked"] == "S", "Embarked"] = 0
titanic.loc[titanic["Embarked"] == "C", "Embarked"] = 1
titanic.loc[titanic["Embarked"] == "Q", "Embarked"] = 2
# print(titanic.describe())
# print(titanic.head())

from sklearn.linear_model import LinearRegression
from sklearn.model_selection import KFold

# predictors = ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked"]
#
# alg = LinearRegression()
#
# kf = KFold(3, shuffle=False)
#
# predictions = []
#
# for train, test in kf.split(titanic):
#     # print(train, test)
#
#     train_predictors = titanic[predictors].iloc[train, :]
#     # The target we're using to train the algorithm.
#     train_target = titanic['Survived'].iloc[train]
#     # Training the algorithm using the predictors and target
#     alg.fit(train_predictors, train_target)
#
#     test_predictions = alg.predict(titanic[predictors].iloc[test, :])
#     predictions.append(test_predictions)

predictors = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked']

# Initialize our algorithm class
# alg = LinearRegression()

# Generate cross validation folds for the titanic dataset. It return the row indices
# corresponding(相应的) to train and test.
# 为Titanic数据集生成交叉验证折叠。它返回与训练和测试相对应的行索引。
# We set random_state to ensure we get the same splits every time we run this.
# kf = KFold(titanic.shape[0], n_folds=3, random_state=1) 写法错误已被弃用

# 样本平均分成3份，3折交叉验证
# kf = KFold(n_splits=3, shuffle=False)
#
# # 注意这里不是kf.split(titanic.shape[0])，会报如下错误：
# # Singleton array array(891) cannot be considered a valid collection.
#
# predictions = []   # 交叉验证 划分训练集 验证集
# for train, test in kf.split(titanic):
#     # The predictors we're using the train the algorithm. Note how we only take
#     # the rows in the train folds
#     # 注意我们只得到训练集的rows
#     train_predictors = titanic[predictors].iloc[train, :]
#     # The target we're using to train the algorithm.
#     train_target = titanic['Survived'].iloc[train]
#     # Training the algorithm using the predictors and target
#     alg.fit(train_predictors, train_target)
#     # We can now make predictions on the test fold
#     test_predictions = alg.predict(titanic[predictors].iloc[test, :])
#     predictions.append(test_predictions)
#
#
# predictions = np.concatenate(predictions, axis=0)
# predictions[predictions >   .5] = 1
# predictions[predictions <=  .5] = 0
# print(predictions)
# print(len(predictions))
# accuracy = sum(predictions[predictions == titanic["Survived"]]) / len(predictions)
# accuracy = sum(predictions == titanic['Survived']) / len(predictions)

# print(accuracy)

# from sklearn.model_selection import cross_val_score
# from sklearn.linear_model import LogisticRegression
#
# alg = LogisticRegression(random_state=1)
# # Compute the accuracy score for all the cross validation folds,
# # (much simper than what we did before!)
# scores = cross_val_score(alg, titanic[predictors], titanic["Survived"], cv=3)
# # Take the mean of the scores (because we have one for each fold)
# print(scores.mean())

from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier


# Initialize our algorithm with the default paramters
# n_estimators is the number of trees we want to make
# min_samples_split is the minimum number of rows we need to make a split
# min_samples_leaf is the minimum number of samples we can have at the place where a
# tree branch(分支) ends (the bottom points of the tree)
# alg = RandomForestClassifier(random_state=1,
#                              n_estimators=10,
#                              min_samples_split=2,
#                              min_samples_leaf=1)

alg = RandomForestClassifier(random_state=1,
                             n_estimators=100,
                             min_samples_split=4,
                             min_samples_leaf=2)
# Compute the accuracy score for all the cross validation folds.  (much simpler than what we did before!)
kf = KFold(n_splits=3, shuffle=False)
scores = cross_val_score(alg, titanic[predictors], titanic["Survived"], cv=kf)

# Take the mean of the scores (because we have one for each fold)
print(scores.mean())


