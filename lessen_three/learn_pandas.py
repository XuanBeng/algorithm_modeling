import pandas as pd
import matplotlib.pyplot as plt



# q1: dataframe，series和ndarray是存在什么关系
# ndaarray 是一个多维数组对象，由元数据（用于描述实际数据）和实际数据组成
# series 可以被简单的认为是一维数组，但与一维数组区别在与series具有索引
# dataframe 是将数个 Series 按列合并而成的二维数据结构，每一列单独取出来是一个 Series
# 关系： dataFrame取一列是series，series取一个是ndarray

# Q2.series中sort_index和sort_values区别是什么？
# sort_values必须指定参数，sort_index有默认参数
# sort_index默认对行列进行排序，其它方式用sort_values

# Q3.DataFrame是否可以用string进行索引，哪个函数可以实现？
# 可以，使用set_index方法实现

# Q4.常用的可视化的库叫什么名字？
#pip install matplotlib

# Q5.在plt.plot(first_twelve['DATE'],first_twelve['VALUE'])中x轴用的哪个数据？y轴用的哪个数据？
# x轴: first_twelve['DATE'] y轴:first_twelve['VALUE']

unrate = pd.read_csv('../static/UNRATE.csv')
unrate['DATE'] = pd.to_datetime(unrate['DATE'])
# print(unrate.head(12))

f = unrate[0 : 12]
plt.plot(f['DATE'], f['VALUE'])
plt.xticks(rotation = 45)
plt.show()

# Q6.线性回归数学模型中为什么要增加全为1的一列？
# 使之符合矩阵运算的格式且不对原数据造成影响

# Q7.在线性回归拟合中，是否允许真实值和预测值之间误差的存在？所存在的误差具有哪些特性？
# 允许； 特性： 独立同分布 服从均值为0方差为O^2的高斯分布

# Q8.请说明高斯分布的表达式的参数含义。
# 正态分布

# Q9.请解释似然函数。
# 什么样的参数跟我们的数据组合后恰好是真实值

